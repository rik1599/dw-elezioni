import argparse

from etl.json_converter import JsonConverter


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'src', help='Path alla directory con i file JSON', type=str)
    parser.add_argument(
        'output_format', help='Formato dell\'output (csv o sqlite)', type=str)
    parser.add_argument('dest', help='Path alla directory di output', type=str)

    args = parser.parse_args()

    jc = JsonConverter(args.src)
    jc.clean()

    if args.output_format == 'csv':
        jc.save_to_csv(args.dest)

    elif args.output_format == 'sqlite':
        jc.save_to_sqlite(args.dest)

    else:
        raise ValueError('Output non definito')


if __name__ == "__main__":
    main()
