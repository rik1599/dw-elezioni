# pylint: disable=redefined-outer-name

import pytest
from selenium import webdriver
from eligendo_downloader.downloader_base import set_type, get_data_elezioni_in_range
from eligendo_downloader.eligendo_downloader_v2 import EligendoDownloaderV2Camera


@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture(autouse=True)
def url(driver: webdriver.Remote):
    driver.get('https://elezionistorico.interno.gov.it/index.php')
    return set_type(driver, 'Senato')


@pytest.fixture
def data(driver: webdriver.Remote):
    options = get_data_elezioni_in_range(driver, 2008, 2022)
    return options[1]


@pytest.fixture
def downloader(driver: webdriver.Remote, url: str, data: str):
    return EligendoDownloaderV2Camera(driver, url, data)


def test(downloader: EligendoDownloaderV2Camera):
    downloader.download()
    downloader.to_json("data/")
