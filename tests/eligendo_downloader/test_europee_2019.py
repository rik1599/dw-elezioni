# pylint: disable=redefined-outer-name

import pytest
from selenium import webdriver
from eligendo_downloader.downloader_base import set_type, get_data_elezioni_in_range
from eligendo_downloader.eligendo_downloader_europee import EligendoDownloaderEuropee


@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture(autouse=True)
def url(driver: webdriver.Remote):
    driver.get('https://elezionistorico.interno.gov.it/index.php')
    return set_type(driver, 'Europee')


@pytest.fixture
def data(driver: webdriver.Remote):
    options = get_data_elezioni_in_range(driver, 2009, 2009)
    return options[0]


@pytest.fixture
def downloader(driver: webdriver.Remote, url: str, data: str):
    return EligendoDownloaderEuropee(driver, url, data)


def test(downloader: EligendoDownloaderEuropee):
    downloader.download()
    downloader.to_json("data/")
    