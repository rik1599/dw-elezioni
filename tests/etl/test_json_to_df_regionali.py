# pylint: disable=redefined-outer-name

from datetime import date

import pandas as pd
import pytest

from etl.json_to_df import ElectionData, JsonToDfRegionali

@pytest.fixture
def election_data():
    return ElectionData('regionali', date.fromisoformat('2018-04-29'), pd.read_json('data/regionali_2018-04-29.json'))

def test_json_to_df_regionali(election_data: ElectionData):
    converter = JsonToDfRegionali(election_data, pd.DataFrame())
    converter.json_to_df()
    converter.risultati_elettorali.to_csv("clean/risultati_elettoriali.csv")
    converter.elezioni.to_csv("clean/elezioni.csv")
    converter.anagrafica.to_csv("clean/anagrafica.csv")
