# pylint: disable=redefined-outer-name

from datetime import date

import pandas as pd
import pytest

from etl.json_to_df import ElectionData, JsonToDfComunali


@pytest.fixture
def election_data():
    return ElectionData('comunali', date.fromisoformat('2009-06-06'), pd.read_json('data/comunali_2009-06-06.json'))

def test_json_to_df_comunali(election_data: ElectionData):
    converter = JsonToDfComunali(election_data, pd.DataFrame())
    converter.json_to_df()
    converter.risultati_elettorali.to_csv("clean/risultati_elettoriali.csv")
    converter.elezioni.to_csv("clean/elezioni.csv")
    converter.anagrafica.to_csv("clean/anagrafica.csv")
