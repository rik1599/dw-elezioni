# pylint: disable=redefined-outer-name

from etl.json_converter import JsonConverter

def test_json_to_csv():
    converter = JsonConverter('data/')
    converter.clean()
    converter.save_to_csv('clean/')
