# pylint: disable=redefined-outer-name

from datetime import date

import pandas as pd
import pytest

from etl.json_to_df import ElectionData, JsonToDfEuropee

@pytest.fixture
def election_data():
    return ElectionData('europee', date.fromisoformat('2014-05-25'), pd.read_json('data/europee_2014-05-25.json'))

def test_json_to_df_europee(election_data: ElectionData):
    converter = JsonToDfEuropee(election_data, pd.DataFrame())
    converter.json_to_df()
    converter.risultati_elettorali.to_csv("clean/risultati_elettoriali.csv")
    converter.elezioni.to_csv("clean/elezioni.csv")
    converter.anagrafica.to_csv("clean/anagrafica.csv")