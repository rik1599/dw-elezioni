# pylint: disable=redefined-outer-name

from datetime import date

import pandas as pd
import pytest

from etl.json_to_df import ElectionData, JsonToDfPolitiche2008, JsonToDfPolitiche2018

@pytest.fixture
def election_data_2013():
    return ElectionData('camera', date.fromisoformat('2013-02-24'), pd.read_json('data/camera_2013-02-24.json'))

@pytest.fixture
def election_data_2018():
    return ElectionData('camera', date.fromisoformat('2018-03-04'), pd.read_json('data/camera_2018-03-04.json'))

def test_json_to_df_politiche_old(election_data_2013: ElectionData):
    converter = JsonToDfPolitiche2008(election_data_2013, pd.DataFrame(), 'CAMERA')
    converter.json_to_df()
    converter.risultati_elettorali.to_csv("clean/risultati_elettoriali.csv")
    converter.elezioni.to_csv("clean/elezioni.csv")
    converter.anagrafica.to_csv("clean/anagrafica.csv")

def test_json_to_df_politiche(election_data_2018: ElectionData):
    converter = JsonToDfPolitiche2018(election_data_2018, pd.DataFrame(), 'CAMERA')
    converter.json_to_df()
    converter.risultati_elettorali.to_csv("clean/risultati_elettoriali.csv")
    converter.elezioni.to_csv("clean/elezioni.csv")
    converter.anagrafica.to_csv("clean/anagrafica.csv")
