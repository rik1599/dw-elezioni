# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v2 import FvgElectionsDownloaderV2Comunali
from fvg_downloader.estrai_comuni import EstraiComuniFromHref
from fvg_downloader.estrai_affluenza import EstraiAffluenzaFromTable
from fvg_downloader.estrai_presidente import EstraiListeCollegateFromP, EstraiPresidenteFromTable
from fvg_downloader.estrai_liste import EstraiListeFromTable

@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture
def url():
    return 'http://elezionistorico.regione.fvg.it/elezioni2013/elezCom.html'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    estrai_comuni = EstraiComuniFromHref(driver)
    estrai_affluenza = EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1)
    estrai_liste = EstraiListeFromTable(driver)
    estrai_sindaco = EstraiPresidenteFromTable(driver, EstraiListeCollegateFromP())
    return FvgElectionsDownloaderV2Comunali(driver, url, estrai_comuni, estrai_liste, estrai_sindaco, estrai_affluenza, '2013-04-21', '2013-05-05')

def test_dati_elezione_comuni(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV2Comunali, url: str):
    driver.get(url)
    downloader.estrai_dati_elezione_comune(('Udine 1° turno', 'http://elezionistorico.regione.fvg.it/elezioni2013/000385_Com/Coalizioni/000387.html'))
    assert_that(downloader.dati_primo_turno).is_length(1)
    assert_that(downloader.dati_ballottaggio).is_length(0)
    downloader.estrai_dati_elezione_comune(('Udine ballottaggio', 'http://elezionistorico.regione.fvg.it/elezioni2013/000386_Com/Coalizioni/000387.html'))
    assert_that(downloader.dati_ballottaggio).is_length(1)

def test_all(downloader: FvgElectionsDownloaderV2Comunali):
    directory = "data/"
    downloader.download()
    downloader.to_json(directory)
    assert_that("data/comunali_2013-04-21.json").exists()
    assert_that("data/comunali_2013-05-05.json").exists()
    