# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v3 import FvgElectionsDownloaderV3Comunali
from fvg_downloader.estrai_comuni import EstraiComuniFromHref
from fvg_downloader.estrai_affluenza import EstraiAffluenzaFromTable
from fvg_downloader.estrai_presidente import EstraiListeCollegateFromP, EstraiPresidenteFromTable
from fvg_downloader.estrai_liste import EstraiListeFromTable

@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture
def url():
    return 'http://elezionistorico.regione.fvg.it/elezioni2009/elezCom.html'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    estrai_comuni = EstraiComuniFromHref(driver)
    estrai_affluenza = EstraiAffluenzaFromTable(driver, elettori_index=1, votanti_index=14)
    estrai_liste = EstraiListeFromTable(driver)
    estrai_sindaco = EstraiPresidenteFromTable(driver, EstraiListeCollegateFromP())
    return FvgElectionsDownloaderV3Comunali(driver, url, estrai_comuni, estrai_liste, estrai_sindaco, estrai_affluenza, '2011-05-15')


def test_dati_elezione_comuni(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV3Comunali, url: str):
    driver.get(url)
    downloader.estrai_dati_elezione_comune(('Fontanafredda', 'http://elezionistorico.regione.fvg.it/elezioni2009/000273_Com/Candidature/000525.html'))
    assert_that(downloader.dati_elezioni).is_length(1)
    assert_that(downloader.dati_elezioni).contains_key('Fontanafredda')

def test_all(downloader: FvgElectionsDownloaderV3Comunali):
    directory = "data/"
    downloader.download()
    downloader.to_json(directory)
    assert_that("data/comunali_2011-05-15.json").exists()
    