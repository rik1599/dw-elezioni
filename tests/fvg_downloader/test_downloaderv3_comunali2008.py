# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v3 import FvgElectionsDownloaderV3Comunali
from fvg_downloader.estrai_comuni import EstraiComuniFromSelectV2
from fvg_downloader.estrai_affluenza import EstraiAffluenzaFromTable
from fvg_downloader.estrai_presidente import EstraiListeCollegateFromDiv, EstraiPresidenteFromDiv
from fvg_downloader.estrai_liste import EstraiListeFromDiv

@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture
def url():
    return 'http://elezionistorico.regione.fvg.it/elezioni2008/elezCom.html'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    estrai_comuni = EstraiComuniFromSelectV2(driver)
    estrai_affluenza = EstraiAffluenzaFromTable(driver, elettori_index=1, votanti_index=14)
    estrai_liste = EstraiListeFromDiv(driver)
    estrai_sindaco = EstraiPresidenteFromDiv(driver, EstraiListeCollegateFromDiv())
    return FvgElectionsDownloaderV3Comunali(driver, url, estrai_comuni, estrai_liste, estrai_sindaco, estrai_affluenza, '2011-05-15')


def test_dati_elezione_comuni(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV3Comunali, url: str):
    driver.get(url)
    downloader.estrai_dati_elezione_comune(('Udine', 'http://elezionistorico.regione.fvg.it/elezioni2008/000246_Com/Candidature/000387.html'))
    assert_that(downloader.dati_elezioni).is_length(1)
    assert_that(downloader.dati_elezioni).contains_key('Udine')

def test_all(downloader: FvgElectionsDownloaderV3Comunali):
    directory = "data/"
    downloader.download()
    downloader.to_json(directory)
    assert_that("data/comunali_2011-05-15.json").exists()
    