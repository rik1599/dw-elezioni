# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v2 import FvgElectionsDownloaderV2Regionali

from fvg_downloader.estrai_comuni import EstraiComuniFromSelect
from fvg_downloader.estrai_affluenza import EstraiAffluenzaFromTable, EstraiProvinceFromSelectV1
from fvg_downloader.estrai_presidente import EstraiPresidenteFromTable, EstraiListeCollegateFromP
from fvg_downloader.estrai_liste import EstraiListeFromTable

@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture
def url():
    return 'http://elezionistorico.regione.fvg.it/elezioni2013/000377_Reg/Coalizioni/000001.html'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    estrai_comuni = EstraiComuniFromSelect(driver)
    estrai_liste = EstraiListeFromTable(driver)
    estrai_presidente = EstraiPresidenteFromTable(driver, EstraiListeCollegateFromP())
    estrai_affluenza = EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1)
    estrai_circoscrizioni = EstraiProvinceFromSelectV1(driver)
    return FvgElectionsDownloaderV2Regionali(driver, url, estrai_comuni, estrai_liste, estrai_presidente, estrai_affluenza, '2013-04-21', estrai_circoscrizioni)

def test_dati_elezione_comuni(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV2Regionali, url: str):
    driver.get(url)
    downloader.estrai_dati_elezione_comune(('Trieste', 'http://elezionistorico.regione.fvg.it/elezioni2013/000377_Reg/Coalizioni/032_006.html'))
    assert_that(downloader.dati_elezioni).is_length(1)
    assert_that(downloader.dati_elezioni).contains_key('Trieste')

def test_dati_affluenza(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV2Regionali):
    driver.get(downloader.base_url)
    downloader.estrai_affluenza_regionali()
    assert_that(downloader.dati_elezioni).is_length(218)
    assert_that(downloader.dati_elezioni['Trieste']).contains_key('affluenza')