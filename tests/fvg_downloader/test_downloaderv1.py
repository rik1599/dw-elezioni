# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v1 import FvgElectionsDownloaderV1


@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()

@pytest.fixture
def url():
    return 'https://elezioni.regione.fvg.it/consultazioni/ELZ_COM/2021-10-03'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    return FvgElectionsDownloaderV1(
        driver,
        url,
        '2021-10-03',
        '2021-10-17'
    )

def test_dati_elezione_comuni(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV1, url: str):
    driver.get(url)
    downloader.estrai_dati_elezione_comune('San Vito al Tagliamento')
    assert_that(downloader.dati_primo_turno).is_length(1)
    assert_that(downloader.dati_ballottaggio).is_length(1)

def test_all(downloader: FvgElectionsDownloaderV1):
    directory = "data/"
    downloader.download()
    downloader.to_json(directory)
    assert_that('data/comunali_2022-06-12.json').exists()
    assert_that('data/comunali_2022-06-26.json').exists()
