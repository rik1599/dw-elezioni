# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v3 import FvgElectionsDownloaderV3Regionali
from fvg_downloader.estrai_comuni import EstraiComuniFromSelect
from fvg_downloader.estrai_affluenza import EstraiAffluenzaFromTable
from fvg_downloader.estrai_presidente import EstraiListeCollegateFromDiv, EstraiPresidenteFromDiv
from fvg_downloader.estrai_liste import EstraiListeFromDiv

@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture
def url():
    return 'http://elezionistorico.regione.fvg.it/elezioni2008/elezReg.html'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    estrai_comuni = EstraiComuniFromSelect(driver)
    estrai_affluenza = EstraiAffluenzaFromTable(driver, elettori_index=1, votanti_index=14)
    estrai_liste = EstraiListeFromDiv(driver)
    estrai_sindaco = EstraiPresidenteFromDiv(driver, EstraiListeCollegateFromDiv())
    return FvgElectionsDownloaderV3Regionali(driver, url, estrai_comuni, estrai_liste, estrai_sindaco, estrai_affluenza, '2011-05-15')

def test_estrai_presidente(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV3Regionali):
    driver.get(downloader.base_url)
    downloader.estrai_dati("Presidente", downloader.estrai_dati_presidente)

def test_estrai_liste(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV3Regionali):
    driver.get(downloader.base_url)
    downloader.estrai_dati("Risultati liste", downloader.estrai_dati_risultati_liste)

def test_estrai_affluenza(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV3Regionali):
    driver.get(downloader.base_url)
    downloader.estrai_affluenza_regionali()

def test_all(downloader: FvgElectionsDownloaderV3Regionali):
    directory = "data/"
    downloader.download()
    downloader.to_json(directory)
    assert_that("data/regionali_2011-05-15.json").exists()
    