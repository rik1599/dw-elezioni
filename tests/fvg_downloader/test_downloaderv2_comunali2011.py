# pylint: disable=redefined-outer-name

import pytest
from assertpy import assert_that
from selenium import webdriver

from fvg_downloader.elezioni_fvg_v2 import FvgElectionsDownloaderV2Comunali
from fvg_downloader.estrai_comuni import EstraiComuniFromHref
from fvg_downloader.estrai_affluenza import EstraiAffluenzaFromTable
from fvg_downloader.estrai_presidente import EstraiListeCollegateFromP, EstraiPresidenteFromTable
from fvg_downloader.estrai_liste import EstraiListeFromTable

@pytest.fixture
def driver():
    selenium_wedriver = webdriver.Remote(
        command_executor='http://selenium:4444',
        options=webdriver.FirefoxOptions()
    )
    yield selenium_wedriver
    selenium_wedriver.quit()


@pytest.fixture
def url():
    return 'http://elezionistorico.regione.fvg.it/amministrative2011/elezCom.html'

@pytest.fixture
def downloader(driver: webdriver.Remote, url: str):
    estrai_comuni = EstraiComuniFromHref(driver)
    estrai_affluenza = EstraiAffluenzaFromTable(driver, elettori_index=1, votanti_index=14)
    estrai_liste = EstraiListeFromTable(driver)
    estrai_sindaco = EstraiPresidenteFromTable(driver, EstraiListeCollegateFromP())
    return FvgElectionsDownloaderV2Comunali(driver, url, estrai_comuni, estrai_liste, estrai_sindaco, estrai_affluenza, '2011-05-15', '2011-05-29')


def test_dati_elezione_comuni(driver: webdriver.Remote, downloader: FvgElectionsDownloaderV2Comunali, url: str):
    driver.get(url)
    downloader.estrai_dati_elezione_comune(('Trieste 1° turno', 'http://elezionistorico.regione.fvg.it/amministrative2011/000309_Com/Candidature/000157.html'))
    assert_that(downloader.dati_primo_turno).is_length(1)
    assert_that(downloader.dati_ballottaggio).is_length(0)
    downloader.estrai_dati_elezione_comune(('Trieste ballottaggio', 'http://elezionistorico.regione.fvg.it/amministrative2011/000323_Com/Affluenza/000157.html'))
    assert_that(downloader.dati_ballottaggio).is_length(1)

def test_all(downloader: FvgElectionsDownloaderV2Comunali):
    directory = "data/"
    downloader.download()
    downloader.to_json(directory)
    assert_that("data/comunali_2013-04-21.json").exists()
    assert_that("data/comunali_2013-05-05.json").exists()
    