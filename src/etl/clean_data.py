import re
from unidecode import unidecode


class StringCleaner:

    def __init__(self, string) -> None:
        self.cleaned = string

    def normalize(self):
        """
        Normalizza le stringhe:
        1) Converte i codici HTML in caratteri e converte i caratteri accentati
        2) Capitalizza la stringa
        3) Rimuove gli apostrofi usati al posto dell'accento
        """
        unescaped: str = unidecode(self.cleaned)
        unescaped = unescaped.upper().strip()

        # Rimuove i doppi apici
        unescaped = re.sub(r"\"", "", unescaped)

        # Rimuove eventuali spazi aggiuntivi
        unescaped = re.sub(r" +", " ", unescaped)

        # Sostuisce i backtick ` con l'apice corretto
        unescaped = re.sub(r"`", "'", unescaped)

        # Rimuove i caratteri accentati
        unescaped = re.sub(r"A'", "A", unescaped)
        unescaped = re.sub(r"E'", "E", unescaped)
        unescaped = re.sub(r"I'", "I", unescaped)
        unescaped = re.sub(r"O'", "O", unescaped)
        unescaped = re.sub(r"U'", "U", unescaped)

        self.cleaned = unescaped
        return self

    def remove_translations(self):
        """
        Rimuove eventuali traduzioni dai nomi dei comuni bilingue o varianti dei nomi
        """
        aliases = {
            "BUJA": "BUIA",
            "TERZO D'AQUILEIA": "TERZO DI AQUILEIA",
            "CHIOPRIS-VISCONE": "CHIOPRIS VISCONE"
        }

        self.cleaned = aliases.get(self.cleaned, self.cleaned)
        self.cleaned = re.sub(r"(\/|-)( *|).*", "", self.cleaned).strip()
        return self
