import os
import re
import sqlite3
from datetime import date
from pathlib import Path

import pandas as pd

import etl.json_to_df as jtod


class JsonConverter:
    """
    Data una directory contenente i file json delle elezioni le converte in dataframe per essere esportate in diversi formati
    """

    def __init__(self, data_path: str) -> None:
        self.risultati_elettorali = pd.DataFrame()
        self.anagrafica = pd.DataFrame()
        self.elezioni = pd.DataFrame()

        self.__cwd = os.getcwd()
        self.__data_path = data_path
        self.__elections_data = []

    def __get_info_elezioni(self, path: str) -> tuple[str, date]:
        data_elezioni_str = re.search(
            r"(19|20)\d{2}-(0|1)\d-\d{2}", path).group()
        data_elezioni = date.fromisoformat(data_elezioni_str)
        tipo_elezioni = re.search(
            r"comunali|regionali|europee|camera|senato", path, re.IGNORECASE).group()
        return (tipo_elezioni, data_elezioni)

    def __dispatch(self, election: jtod.ElectionData) -> jtod.JsonToDf:
        if election.tipo == 'comunali':
            return jtod.JsonToDfComunali(election, self.anagrafica)
        if election.tipo == 'regionali':
            return jtod.JsonToDfRegionali(election, self.anagrafica)
        if election.tipo == 'europee':
            return jtod.JsonToDfEuropee(election, self.anagrafica)
        if election.tipo in ['camera', 'senato'] and 2008 <= election.data.year <= 2013:
            return jtod.JsonToDfPolitiche2008(election, self.anagrafica)
        if election.tipo in ['camera', 'senato'] and election.data.year >= 2018:
            return jtod.JsonToDfPolitiche2018(election, self.anagrafica)
        return None

    def __get_election_data(self):
        self.__elections_data = [
            jtod.ElectionData(*self.__get_info_elezioni(file),
                              pd.read_json(file))
            for file in os.listdir() if file.endswith('.json')
        ]

    def clean(self):
        os.chdir(self.__data_path)
        self.__get_election_data()

        for election in self.__elections_data:
            print(f"{election.tipo} del {election.data}")
            converter = self.__dispatch(election)
            converter.json_to_df()
            self.risultati_elettorali = pd.concat(
                [self.risultati_elettorali, converter.risultati_elettorali], ignore_index=True)
            self.elezioni = pd.concat(
                [self.elezioni, converter.elezioni], ignore_index=True)
            self.anagrafica = converter.anagrafica

        self.risultati_elettorali['candidato'] = self.risultati_elettorali['candidato'].astype(
            dtype='Int64')
        self.risultati_elettorali['candidato_sostenuto'] = self.risultati_elettorali['candidato_sostenuto'].astype(
            dtype='Int64')
        self.elezioni['votanti'] = self.elezioni['votanti'].astype(
            dtype='Int64')
        self.elezioni['elettori'] = self.elezioni['elettori'].astype(
            dtype='Int64')
        self.elezioni['schede_bianche'] = self.elezioni['schede_bianche'].astype(
            dtype='Int64')
        self.elezioni['schede_nulle'] = self.elezioni['schede_nulle'].astype(
            dtype='Int64')
        self.elezioni['voti_contestati'] = self.elezioni['voti_contestati'].astype(
            dtype='Int64')

        os.chdir(self.__cwd)

    @property
    def anagrafica_without_s_name(self):
        cpy = self.anagrafica.copy(deep=False)
        del cpy['s_name']
        return cpy

    def save_to_csv(self, save_path):
        """
        Salva i dati del dw in 3 file csv:
            - risultati_elettorali.csv
            - elezioni.csv
            - anagrafica.csv
        @params save_path: directory in cui salvare i tre file
        """

        params = {
            'index': True,
            'index_label': 'id',
            'chunksize': 512
        }

        p = Path(save_path)
        self.risultati_elettorali.to_csv(p / 'risultati_elettorali.csv', **params)
        self.elezioni.to_csv(p / 'elezioni.csv', **params)
        self.anagrafica_without_s_name.to_csv(p / 'anagrafica.csv', **params)

    def save_to_sqlite(self, save_path):
        p = Path(save_path)
        conn = sqlite3.connect(p / 'dw-elezioni.sqlite')
        params = {
            'con': conn,
            'if_exists': 'replace',
            'index': True,
            'index_label': 'id',
            'chunksize': 512,
            'method': 'multi'
        }

        self.risultati_elettorali.to_sql('risultati_elettorali', **params)
        self.elezioni.to_sql('elezioni', **params)
        self.anagrafica_without_s_name.to_sql('anagrafica', **params)

        conn.close()
