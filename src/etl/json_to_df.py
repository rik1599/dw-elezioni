from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import date

import pandas as pd

from etl.clean_data import StringCleaner


@dataclass
class ElectionData:
    """
    Dataclass per memorizzare i dati grezzi
    """
    tipo: str
    data: date
    json: pd.DataFrame


@dataclass
class TempLists:

    header: dict
    vals: pd.Series
    list_risultati_elettorali: list
    list_elezioni: list

    def __init__(self) -> None:
        self.header = {}
        self.vals = pd.Series(dtype='object')
        self.list_risultati_elettorali = []
        self.list_elezioni = []


class JsonToDf(ABC):
    def __init__(self, election_data: ElectionData, anagrafica: pd.DataFrame) -> None:
        self.election_data = election_data
        self.anagrafica = anagrafica
        # check if exists "s_name" in anagrafica
        if 's_name' not in self.anagrafica:
            self.anagrafica['s_name'] = []

        self.risultati_elettorali = pd.DataFrame()
        self.elezioni = pd.DataFrame()

    def update_anagrafica(self, nome: str) -> int:
        """
        Dato il nome di un candidato lo inserisce nell'anagrafica.
        Se un candidato esiste già ritorna l'index del candidato.
        Torna None se il candidato ha un nome non valido
        """
        normalized = StringCleaner(nome).normalize().cleaned

        if normalized in ['-', 'CANDIDATO NON PRESENTE']:
            return None

        splitted_name = normalized.split()
        splitted_name.sort()
        sorted_name = " ".join(splitted_name)
        search = self.anagrafica.query('s_name == @sorted_name')
        if len(search) == 0:
            self.anagrafica = pd.concat(
                [self.anagrafica, pd.DataFrame([{
                    's_name': sorted_name,
                    'nome': StringCleaner(nome).normalize().cleaned
                }])], ignore_index=True)
            return int(self.anagrafica.last_valid_index())

        return int(search.last_valid_index())

    @abstractmethod
    def json_to_df(self):
        pass


class JsonToDfComunali(JsonToDf):
    """
    Converte i dati delle elezioni comunali
    """

    def clean_sindaco(self, args: TempLists):
        args.list_elezioni.append({
            **args.header,
            'organo_elezione': 'SINDACO',
            'votanti': int(args.vals.affluenza['votanti']),
            'elettori': int(args.vals.affluenza['elettori']),
            'schede_bianche': int(args.vals.sindaco['schede_bianche']),
            'schede_nulle': int(args.vals.sindaco['schede_nulle']),
            'voti_contestati': int(args.vals.sindaco['voti_contestati'])
        })

        liste_collegate = {}
        for candidato, data in args.vals.sindaco['voti_validi'].items():
            id_candidato = self.update_anagrafica(candidato)
            liste_collegate = liste_collegate | {
                StringCleaner(lista).normalize().cleaned: id_candidato for lista in data['liste_collegate']}
            args.list_risultati_elettorali.append({
                **args.header,
                'organo_elezione': 'SINDACO',
                'candidato': id_candidato,
                'num_voti': int(data['voti'])
            })
        return liste_collegate

    def clean_liste(self, args: TempLists, liste_collegate: dict):
        args.list_elezioni.append({
            **args.header,
            'organo_elezione': 'CONSIGLIO',
            'votanti': int(args.vals.affluenza['votanti']),
            'elettori': int(args.vals.affluenza['elettori']),
            'schede_bianche': int(args.vals.sindaco['schede_bianche']),
            'schede_nulle': int(args.vals.sindaco['schede_nulle']),
            'voti_contestati': int(args.vals.liste['voti_contestati'])
        })
        for lista, data in args.vals.liste['voti_validi'].items():
            tot_voti = int(data['voti'])
            for candidato, voti in data['preferenze'].items():
                id_candidato = self.update_anagrafica(candidato)
                if id_candidato is None:
                    continue

                args.list_risultati_elettorali.append({
                    **args.header,
                    'organo_elezione': 'CONSIGLIO',
                    'candidato': id_candidato,
                    'lista': StringCleaner(lista).normalize().cleaned,
                    'candidato_sostenuto': liste_collegate.get(StringCleaner(lista).normalize().cleaned, None),
                    'num_voti': int(voti)
                })
                tot_voti = tot_voti - int(voti)

            args.list_risultati_elettorali.append({
                **args.header,
                'organo_elezione': 'CONSIGLIO',
                'lista': StringCleaner(lista).normalize().cleaned,
                'candidato_sostenuto': liste_collegate.get(StringCleaner(lista).normalize().cleaned, None),
                'num_voti': tot_voti
            })

    def json_to_df(self):
        temp = TempLists()
        for comune, vals in self.election_data.json.items():
            temp.header = {
                'giorno': self.election_data.data, 'anno': self.election_data.data.year,
                'comune': StringCleaner(comune).normalize().remove_translations().cleaned,
                'tipo_elezione': 'COMUNALI' if hasattr(vals, 'liste') else 'BALLOTTAGGIO'
            }
            temp.vals = vals

            liste_collegate = self.clean_sindaco(temp)

            if hasattr(vals, 'liste'):
                self.clean_liste(temp, liste_collegate)

        self.elezioni = pd.DataFrame(temp.list_elezioni)
        self.risultati_elettorali = pd.DataFrame(
            temp.list_risultati_elettorali)


class JsonToDfRegionali(JsonToDf):
    def get_liste_collegate(self, candidati: dict):
        liste_collegate = {}
        for candidato, vals in candidati.items():
            id_candidato = self.update_anagrafica(candidato)
            liste_collegate = liste_collegate | {
                StringCleaner(lista).normalize().cleaned: id_candidato for lista in vals['liste_collegate']
            }
        return liste_collegate

    def clean_presidente(self, args: TempLists):
        args.list_elezioni.append({
            **args.header,
            'organo_elezione': 'PRESIDENTE',
            'votanti': int(args.vals.affluenza['votanti']),
            'elettori': int(args.vals.affluenza['elettori']),
            'schede_bianche': int(args.vals.presidente['schede_bianche']),
            'schede_nulle': int(args.vals.presidente['schede_nulle']),
            'voti_contestati': int(args.vals.presidente['voti_contestati'])
        })
        for candidato, data in args.vals.presidente['voti_validi'].items():
            id_candidato = self.update_anagrafica(candidato)
            args.list_risultati_elettorali.append({
                **args.header,
                'organo_elezione': 'PRESIDENTE',
                'candidato': id_candidato,
                'num_voti': int(data['voti'])
            })

    def clean_liste(self, args: TempLists, liste_collegate: dict):
        args.list_elezioni.append({
            **args.header,
            'organo_elezione': 'CONSIGLIO',
            'votanti': int(args.vals.affluenza['votanti']),
            'elettori': int(args.vals.affluenza['elettori']),
            'schede_bianche': int(args.vals.presidente['schede_bianche']),
            'schede_nulle': int(args.vals.presidente['schede_nulle']),
            'voti_contestati': int(args.vals.liste['voti_contestati'])
        })
        for lista, data in args.vals.liste['voti_validi'].items():
            tot_voti = int(data['voti'])
            for candidato, voti in data['preferenze'].items():
                id_candidato = self.update_anagrafica(candidato)
                args.list_risultati_elettorali.append({
                    **args.header,
                    'organo_elezione': 'CONSIGLIO',
                    'candidato': id_candidato,
                    'lista': StringCleaner(lista).normalize().cleaned,
                    'candidato_sostenuto': liste_collegate.get(StringCleaner(lista).normalize().cleaned, None),
                    'num_voti': int(voti)
                })
                tot_voti = tot_voti - int(voti)

            args.list_risultati_elettorali.append({
                **args.header,
                'organo_elezione': 'CONSIGLIO',
                'lista': StringCleaner(lista).normalize().cleaned,
                'candidato_sostenuto': liste_collegate.get(StringCleaner(lista).normalize().cleaned, None),
                'num_voti': tot_voti
            })

    def json_to_df(self):
        temp = TempLists()

        candidati: dict[str, dict] = self.election_data.json.iloc[:,
                                                                  0].presidente['voti_validi']
        liste_collegate = self.get_liste_collegate(candidati)

        for comune, vals in self.election_data.json.items():
            temp.header = {
                'giorno': self.election_data.data, 'anno': self.election_data.data.year,
                'comune': StringCleaner(comune).normalize().remove_translations().cleaned,
                'tipo_elezione': 'REGIONALI'
            }
            temp.vals = vals

            self.clean_presidente(temp)
            self.clean_liste(temp, liste_collegate)
        self.elezioni = pd.DataFrame(temp.list_elezioni)
        self.risultati_elettorali = pd.DataFrame(
            temp.list_risultati_elettorali)


def clean_affluenza_eligendo(args: TempLists):
    args.list_elezioni.append({
        **args.header,
        'votanti': args.vals.affluenza['votanti'],
        'elettori': args.vals.affluenza['elettori'],
        'schede_bianche': args.vals.schede['bianche'],
        'schede_nulle': args.vals.schede['non_valide']
    })


class JsonToDfPolitiche2008(JsonToDf):

    def clean_liste(self, args: TempLists):
        for lista, voti in args.vals.voti_validi.items():
            args.list_risultati_elettorali.append({
                **args.header,
                'lista': StringCleaner(lista).normalize().cleaned,
                'num_voti': voti
            })

    def json_to_df(self):
        temp = TempLists()
        for comune, vals in self.election_data.json.items():
            temp.header = {
                'giorno': self.election_data.data, 'anno': self.election_data.data.year,
                'comune': StringCleaner(comune).normalize().remove_translations().cleaned,
                'tipo_elezione': 'POLITICHE',
                'organo_elezione': self.election_data.tipo.upper()
            }
            temp.vals = vals

            clean_affluenza_eligendo(temp)
            self.clean_liste(temp)

        self.elezioni = pd.DataFrame(temp.list_elezioni)
        self.risultati_elettorali = pd.DataFrame(
            temp.list_risultati_elettorali)


class JsonToDfPolitiche2018(JsonToDf):

    def clean_liste(self, args: TempLists):
        for candidato, risultati in args.vals.voti_validi.items():
            id_candidato = self.update_anagrafica(candidato)
            if id_candidato is None:
                continue

            args.list_risultati_elettorali.append({
                **args.header,
                'candidato': id_candidato,
                'num_voti': int(risultati['voti'])
            })

            for lista, voti in risultati['liste'].items():
                args.list_risultati_elettorali.append({
                    **args.header,
                    'lista': StringCleaner(lista).normalize().cleaned,
                    'candidato_sostenuto': id_candidato,
                    'num_voti': int(voti)
                })

    def json_to_df(self):
        temp = TempLists()
        for comune, vals in self.election_data.json.items():
            temp.header = {
                'giorno': self.election_data.data, 'anno': self.election_data.data.year,
                'comune': StringCleaner(comune).normalize().remove_translations().cleaned,
                'tipo_elezione': 'POLITICHE',
                'organo_elezione': self.election_data.tipo.upper()
            }
            temp.vals = vals

            clean_affluenza_eligendo(temp)
            self.clean_liste(temp)

        self.elezioni = pd.DataFrame(temp.list_elezioni)
        self.risultati_elettorali = pd.DataFrame(
            temp.list_risultati_elettorali)


class JsonToDfEuropee(JsonToDf):

    def clean_liste(self, temp: TempLists):
        for lista, risultati in temp.vals.voti_validi.items():
            tot_voti = int(risultati['voti'])
            for candidato, preferenze in risultati['preferenze'].items():
                id_candidato = self.update_anagrafica(candidato)
                temp.list_risultati_elettorali.append({
                    **temp.header,
                    'lista': StringCleaner(lista).normalize().cleaned,
                    'candidato': id_candidato,
                    'num_voti': int(preferenze)
                })
                tot_voti = tot_voti - int(preferenze)

            temp.list_risultati_elettorali.append({
                **temp.header,
                'lista': StringCleaner(lista).normalize().cleaned,
                'num_voti': tot_voti
            })

    def json_to_df(self):
        temp = TempLists()
        for comune, vals in self.election_data.json.items():
            temp.header = {
                'giorno': self.election_data.data, 'anno': self.election_data.data.year,
                'comune': StringCleaner(comune).normalize().remove_translations().cleaned,
                'tipo_elezione': 'EUROPEE',
                'organo_elezione': 'PARLAMENTO'
            }
            temp.vals = vals

            clean_affluenza_eligendo(temp)
            self.clean_liste(temp)

        self.elezioni = pd.DataFrame(temp.list_elezioni)
        self.risultati_elettorali = pd.DataFrame(
            temp.list_risultati_elettorali)
