"""
Contiene la classe base per implementare i downloader
"""
import json

from abc import ABC, abstractmethod

from selenium.webdriver.remote import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from .estrai_comuni import EstraiComuni
from .estrai_liste import EstraiListe
from .estrai_presidente import EstraiPresidente
from .estrai_affluenza import EstraiAffluenza


class FvgElectionsDownloaderBase(ABC):
    """
    Classe astratta per implementare i downloader
    """

    def __init__(self, driver: webdriver.WebDriver,
                 base_url: str,
                 estrai_comuni: EstraiComuni,
                 estrai_liste: EstraiListe,
                 estrai_presidente: EstraiPresidente,
                 estrai_affluenza: EstraiAffluenza) -> None:
        self.driver = driver
        self.wait = WebDriverWait(driver=driver, timeout=10)
        self.base_url = base_url

        self.estrai_comuni = estrai_comuni
        self.estrai_liste = estrai_liste
        self.estrai_presidente = estrai_presidente
        self.estrai_affluenza = estrai_affluenza

    def download(self) -> None:
        """
        Scarica i dati elettorali dal sito
        """
        self.driver.get(url=self.base_url)
        comuni = self.estrai_comuni.estrai_comuni()
        for comune in comuni:
            self.estrai_dati_elezione_comune(comune=comune)

    @abstractmethod
    def estrai_dati_elezione_comune(self, comune) -> None:
        """
        Estrae i dati elettorali in un certo comune
        """

    @abstractmethod
    def to_json(self, path) -> None:
        """
        Esporta i dati scaricati su un file json al path indicato
        """


class FvgElectionDownloaderComunali(FvgElectionsDownloaderBase, ABC):
    def __init__(self, driver: webdriver.WebDriver, base_url: str,
                 estrai_comuni: EstraiComuni, estrai_liste: EstraiListe, estrai_presidente: EstraiPresidente, estrai_affluenza: EstraiAffluenza,
                 data_primo_turno: str, data_ballottaggio: str) -> None:
        super().__init__(driver, base_url, estrai_comuni,
                         estrai_liste, estrai_presidente, estrai_affluenza)
        self.dati_primo_turno: dict = {}
        self.dati_ballottaggio: dict = {}
        self.data_primo_turno = data_primo_turno
        self.data_ballotaaggio = data_ballottaggio

    def to_json(self, path) -> None:
        dati_elezioni = [self.dati_primo_turno, self.dati_ballottaggio]
        date = [self.data_primo_turno, self.data_ballotaaggio]

        for data, dati_elezione in zip(date, dati_elezioni):
            if len(dati_elezione) == 0:
                continue

            with open(f"{path}comunali_{data}.json", 'w', encoding='UTF-8') as file:
                json.dump(dati_elezione, file, indent=2)


class FvgElectionsDownloaderRegionali(FvgElectionsDownloaderBase, ABC):
    def __init__(self, driver: webdriver.WebDriver, base_url: str,
                 estrai_comuni: EstraiComuni, estrai_liste: EstraiListe,
                 estrai_presidente: EstraiPresidente, estrai_affluenza: EstraiAffluenza,
                 data_elezioni: str) -> None:
        super().__init__(driver, base_url, estrai_comuni,
                         estrai_liste, estrai_presidente, estrai_affluenza)
        self.dati_elezioni: dict = {}
        self.data_elezioni = data_elezioni

    def download(self) -> None:
        super().download()
        self.estrai_affluenza_regionali()

    @abstractmethod
    def estrai_affluenza_regionali(self) -> None:
        pass

    def to_json(self, path) -> None:
        with open(f"{path}regionali_{self.data_elezioni}.json", 'w', encoding='UTF-8') as file:
            json.dump(self.dati_elezioni, file, indent=2)
