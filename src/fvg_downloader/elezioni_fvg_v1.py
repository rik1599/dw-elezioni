"""
Script python per scaricare i dati delle elezioni da
https://elezioni.regione.fvg.it (elezioni dal 2020 al 2022)
(usa selenium)
"""
from selenium.webdriver.common.by import By
from selenium.webdriver.remote import webdriver
from selenium.webdriver.support import expected_conditions as EC

import fvg_downloader.estrai_affluenza as ea
import fvg_downloader.estrai_comuni as ec
import fvg_downloader.estrai_liste as el
import fvg_downloader.estrai_presidente as ep

from .downloader_base import FvgElectionDownloaderComunali

SCROLL_TO_TOP_SCRIPT = 'scroll(0,0);'


class FvgElectionsDownloaderV1(FvgElectionDownloaderComunali):
    """
    Downloader per scaricare i dati elettoriali da
    https://elezioni.regione.fvg.it (elezioni dal 2020 al 2022)
    """

    def __init__(self, driver: webdriver.WebDriver, base_url: str,
                 data_primo_turno: str, data_ballottaggio: str) -> None:

        estrai_comuni = ec.EstraiComuniFromMat(driver)
        estrai_liste = el.EstraiListeFromApp(driver)
        estrai_presidente = ep.EstraiPresidenteFromApp(driver)
        estrai_affluenza = ea.EstraiAffluenzaFromApp(driver)
        super().__init__(driver, base_url, estrai_comuni, estrai_liste,
                         estrai_presidente, estrai_affluenza, data_primo_turno, data_ballottaggio)

    def estrai_dati_elezione_comune(self, comune: str) -> None:
        iniziale = comune[0].capitalize()
        self.wait.until(EC.element_to_be_clickable((
            By.XPATH,
            f'//span[normalize-space()="{iniziale}"]'))
        ).click()
        self.wait.until(EC.element_to_be_clickable((
            By.XPATH,
            f'//div[normalize-space()="{comune}"]'))
        ).click()

        print(comune)
        toogle = self.__ballottaggio()
        if toogle:
            self.dati_ballottaggio[comune] = self.__estrai_dati_elettoriali(
                True)
            self.__scroll_to_top()
            toogle.click()
        self.dati_primo_turno[comune] = self.__estrai_dati_elettoriali()
        self.driver.back()

    def __ballottaggio(self):
        """
        Cerca nella pagina il toggle per i dati del ballottaggio
        """
        self.wait.until(EC.element_to_be_clickable((
            By.XPATH,
            "//div[normalize-space()='SINDACO']"
        )))

        toggle = self.driver.find_elements(By.XPATH, "//mat-slide-toggle")
        if len(toggle) > 0:
            toggle[0].click()
            return toggle[0]
        return None

    def __estrai_dati_elettoriali(self, ballottaggio=False):
        data = {}

        data['sindaco'] = self.estrai_presidente.estrai_presidente()
        self.__scroll_to_top()

        data['affluenza'] = self.estrai_affluenza.estrai_affluenza()

        if not ballottaggio:
            data['liste'] = self.estrai_liste.estrai_liste()
        return data

    def __scroll_to_top(self):
        self.driver.execute_script(SCROLL_TO_TOP_SCRIPT)
        self.wait.until(EC.visibility_of_element_located((
            By.XPATH, "//app-header"
        )))
