"""
Script python per scaricare i dati delle elezioni comunali da
http://elezionistorico.regione.fvg.it (elezioni del 2019)
(usa selenium)
"""
import json

from selenium.webdriver.common.by import By
from selenium.webdriver.remote import webdriver

from .downloader_base import FvgElectionsDownloaderBase
from .estrai_affluenza import EstraiAffluenza, EstraiProvinceFromSelectV2
from .estrai_comuni import EstraiComuni
from .estrai_liste import EstraiListe
from .estrai_presidente import EstraiPresidente
from .elezioni_fvg_v2 import LinksMixin


class FvgElectionsDownloaderV3Comunali(FvgElectionsDownloaderBase):
    """
    Scarica i dati delle elezioni comunali
    """

    def __init__(self, driver: webdriver.WebDriver, base_url: str, estrai_comuni: EstraiComuni, estrai_liste: EstraiListe, estrai_presidente: EstraiPresidente, estrai_affluenza: EstraiAffluenza, data_elezioni: str) -> None:
        super().__init__(driver, base_url, estrai_comuni,
                         estrai_liste, estrai_presidente, estrai_affluenza)
        self.data_elezioni = data_elezioni
        self.dati_elezioni = {}

    def download(self) -> None:
        self.driver.get(self.base_url)

        comuni = self.estrai_comuni.estrai_comuni()
        for comune in comuni:
            self.estrai_dati_elezione_comune(comune=comune)

        self.estrai_affluenza_comunali()

    def estrai_dati_elezione_comune(self, comune: tuple[str, str]) -> None:
        self.driver.get(url=comune[1])
        print(comune[0])
        self.dati_elezioni[comune[0]] = {}

        self.wait.until(lambda d: d.find_element(
            By.XPATH, "//*[@id='testo' or @class='contenitoreTabelle']"))

        link_sindaco_presidente = LinksMixin.link_page_sindaco_presidente(
            self.driver)
        self.dati_elezioni[comune[0]]["sindaco"] = self.estrai_presidente.estrai_presidente(
            url=link_sindaco_presidente)

        link_liste = LinksMixin.link_page_liste(self.driver)
        if link_liste is not None:
            self.dati_elezioni[comune[0]]["liste"] = self.estrai_liste.estrai_liste(
                url=link_liste)

    def estrai_affluenza_comunali(self):
        link_affluenza = LinksMixin.link_page_affluenza(self.driver)
        self.driver.get(link_affluenza)

        estrai_province = EstraiProvinceFromSelectV2(self.driver)

        for link in estrai_province.estrai_province():
            for comune, affluenza in self.estrai_affluenza.estrai_affluenza(url=link).items():
                if self.dati_elezioni.get(comune, None) is None:
                    self.dati_elezioni[comune] = {}

                self.dati_elezioni[comune]['affluenza'] = affluenza

    def to_json(self, path) -> None:
        with open(f"{path}comunali_{self.data_elezioni}.json", 'w', encoding='UTF-8') as file:
            json.dump(self.dati_elezioni, file, indent=2)


class FvgElectionsDownloaderV3Regionali(FvgElectionsDownloaderBase):
    """
    Scarica i dati delle elezioni regionali
    """

    def __init__(self, driver: webdriver.WebDriver, base_url: str, estrai_comuni: EstraiComuni, estrai_liste: EstraiListe, estrai_presidente: EstraiPresidente, estrai_affluenza: EstraiAffluenza, data_elezioni: str) -> None:
        super().__init__(driver, base_url, estrai_comuni,
                         estrai_liste, estrai_presidente, estrai_affluenza)
        self.data_elezioni = data_elezioni
        self.dati_elezioni = {}

    def download(self) -> None:
        self.driver.get(self.base_url)
        self.estrai_dati("Presidente", self.estrai_dati_presidente)
        self.estrai_dati("Risultati liste", self.estrai_dati_risultati_liste)
        self.estrai_affluenza_regionali()


    def estrai_dati(self, tab, action):
        self.wait.until(lambda d: d.find_element(
            By.XPATH, f"//a[contains(text(), '{tab}')]")).click()
        comuni = self.estrai_comuni.estrai_comuni()
        for comune in comuni:
            if comune[0] not in self.dati_elezioni:
                self.dati_elezioni[comune[0]] = {}
            self.driver.get(comune[1])
            print(comune[0])
            action(comune)

    def estrai_dati_presidente(self, comune: tuple[str, str]):
        self.dati_elezioni[comune[0]]['presidente'] = self.estrai_presidente.estrai_presidente(url=comune[1])

    def estrai_dati_risultati_liste(self, comune: tuple[str, str]):
        self.dati_elezioni[comune[0]]['liste'] = self.estrai_liste.estrai_liste(url=comune[1])

    def estrai_affluenza_regionali(self):
        self.wait.until(lambda d: d.find_element(
            By.XPATH, "//a[contains(text(), 'Affluenza')]")).click()

        estrai_province = EstraiProvinceFromSelectV2(self.driver)

        for link in estrai_province.estrai_province():
            for comune, affluenza in self.estrai_affluenza.estrai_affluenza(url=link).items():
                if self.dati_elezioni.get(comune, None) is None:
                    self.dati_elezioni[comune] = {}

                self.dati_elezioni[comune]['affluenza'] = affluenza


    def estrai_dati_elezione_comune(self, comune) -> None:
        """
        In questa implementazione questa funzione è inutile
        """

    def to_json(self, path) -> None:
        with open(f"{path}regionali_{self.data_elezioni}.json", 'w', encoding='UTF-8') as file:
            json.dump(self.dati_elezioni, file, indent=2)
