from abc import ABC, abstractmethod

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

from webscraper import WebScraper


class EstraiListeCollegate(ABC):
    @abstractmethod
    def estrai_liste_collegate(self, container: WebElement) -> list:
        pass


class EstraiListeCollegateFromImgAlt(EstraiListeCollegate):
    def estrai_liste_collegate(self, container: WebElement) -> list:
        return [
            img.get_attribute('alt')
            for img in container.find_elements(By.TAG_NAME, 'img')
        ]


class EstraiListeCollegateFromP(EstraiListeCollegate):
    def estrai_liste_collegate(self, container: WebElement) -> list:
        return [
            p.get_attribute('textContent')
            for p in container.find_elements(By.TAG_NAME, 'p')
        ]

def liste_aliases(text):
    """
    Nei dati elettorali più vecchi nelle coalizioni sono riportate le abbreviazioni delle liste invece del nome completo
    """
    alternatives = {
        "CITTADINI X IL PRES": "CITTADINI PER IL PRESIDENTE",
        "ITALIA DEI VALORI - LISTA DI PIETRO": "ITALIA DEI VALORI",
        "I.D.V.": "ITALIA DEI VALORI",
        "SINISTRA ARCOBALENO": "LA SINISTRA L'ARCOBALENO",
        "PD": "PARTITO DEMOCRATICO",
        "P.D.": "PARTITO DEMOCRATICO",
        "UDC": "UNIONE DEI DEMOCR. CRISTIANI E DEMOCR. DI CENTRO",
        "PDL": "IL POPOLO DELLA LIBERTA'",
        "P.D.L.": "IL POPOLO DELLA LIBERTA'",
        "PDL LEGA NORD": "IL POPOLO DELLA LIBERTA' LEGA NORD PADANIA"
    }
    return alternatives.get(text, text)

class EstraiListeCollegateFromDiv(EstraiListeCollegate):

    def estrai_liste_collegate(self, container: WebElement) -> list:
        return [
            liste_aliases(div.text)
            for div in container.find_elements(By.TAG_NAME, 'div')
        ]

class EstraiPresidente(ABC, WebScraper):
    @abstractmethod
    def estrai_presidente(self, **kwargs):
        pass


class EstraiPresidenteFromApp(EstraiPresidente):
    def estrai_presidente(self, **kwargs):
        self.wait.until(EC.element_to_be_clickable((
            By.XPATH, "//div[normalize-space()='SINDACO']"))).click()

        schede_bianche = self.wait.until(lambda d: d.find_element(
            By.XPATH,
            "//app-comunali-sindaco//child::p[contains(text(), 'schede bianche')]/strong"
        ))
        schede_nulle = self.wait.until(lambda d: d.find_element(
            By.XPATH,
            "//app-comunali-sindaco//child::p[contains(text(), 'schede nulle')]/strong"
        ))
        voti_contestati = self.wait.until(lambda d: d.find_element(
            By.XPATH,
            "//app-comunali-sindaco//child::p[contains(text(), 'voti contestati')]/strong"
        ))

        candidati: list[WebElement] = self.wait.until(lambda d: d.find_elements(By.XPATH, "//*[contains(text(), 'Candidato')]//ancestor::table/tbody/tr"))
        return {
            'schede_bianche': int(schede_bianche.get_attribute("textContent")),
            'schede_nulle': int(schede_nulle.get_attribute("textContent")),
            'voti_contestati': int(voti_contestati.get_attribute("textContent")),
            'voti_validi': {
                cell[0].get_attribute("textContent"): {
                    'voti': int(
                        cell[2].find_element(By.TAG_NAME, 'strong').get_attribute('textContent')),
                    'liste_collegate': EstraiListeCollegateFromImgAlt().estrai_liste_collegate(container=cell[1])
                }

                for row in candidati if (cell := row.find_elements(By.TAG_NAME, "td"))
            }
        }


def estrai_voti_non_validi(driver: WebDriver):
    schede_bianche = driver.find_element(
        By.XPATH,
        "//*[contains(text(), 'Schede bianche')]"
    )

    schede_nulle = driver.find_element(
        By.XPATH,
        "//*[contains(text(), 'Schede nulle')]"
    )

    voti_contestati = driver.find_element(
        By.XPATH,
        "//*[contains(text(), 'Voti contestati')]"
    )

    return (schede_bianche, schede_nulle, voti_contestati)

class EstraiPresidenteFromTable(EstraiPresidente):

    def __init__(self, driver: WebDriver, estrai_liste_collegate: EstraiListeCollegate) -> None:
        super().__init__(driver)
        self.estrai_liste_collegate = estrai_liste_collegate

    def estrai_presidente(self, **kwargs):
        self.driver.get(url=kwargs['url'])
        candidati: list[WebElement] = self.wait.until(lambda d: d.find_elements(
            By.XPATH,
            "//table[contains(@summary, 'candidati')]/tbody/tr"
        ))

        schede_bianche, schede_nulle, voti_contestati = estrai_voti_non_validi(self.driver)

        return {
            'schede_bianche': self.atoi(schede_bianche.get_attribute('textContent')),
            'schede_nulle': self.atoi(schede_nulle.get_attribute('textContent')),
            'voti_contestati': self.atoi(voti_contestati.get_attribute('textContent')),
            'voti_validi': {
                cell[0].text: {
                    'voti': self.atoi(cell[2].text),
                    'liste_collegate': self.estrai_liste_collegate.estrai_liste_collegate(container=cell[1])
                }

                for row in candidati if (cell := row.find_elements(By.TAG_NAME, 'td'))
            }
        }

class EstraiPresidenteFromDiv(EstraiPresidente):

    def __init__(self, driver: WebDriver, estrai_liste_collegate: EstraiListeCollegate) -> None:
        super().__init__(driver)
        self.estrai_liste_collegate = estrai_liste_collegate

    def estrai_presidente(self, **kwargs):
        self.driver.get(url=kwargs['url'])

        schede_bianche, schede_nulle, voti_contestati = estrai_voti_non_validi(self.driver)

        div_candidati = self.driver.find_elements(By.XPATH, "//*[@class='candidatoliste']")
        div_liste = self.driver.find_elements(By.XPATH, "//*[@class='listecollegate']")[1:]
        div_voti = self.driver.find_elements(By.XPATH, "//*[@class='voti']")[1:]

        return {
            'schede_bianche': self.atoi(schede_bianche.get_attribute('textContent')),
            'schede_nulle': self.atoi(schede_nulle.get_attribute('textContent')),
            'voti_contestati': self.atoi(voti_contestati.get_attribute('textContent')),
            'voti_validi': {
                candidato.text: {
                    'voti': self.atoi(voti.text),
                    'liste_collegate': self.estrai_liste_collegate.estrai_liste_collegate(container=liste)
                }
                for candidato, liste, voti in zip(div_candidati, div_liste, div_voti)
            }
        }
