from abc import ABC, abstractmethod

import re
import pandas as pd

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

from webscraper import WebScraper


class EstraiProvince(ABC, WebScraper):
    @abstractmethod
    def estrai_province(self, **kwargs) -> list:
        pass


class EstraiProvinceFromTable(EstraiProvince):
    def estrai_province(self, **kwargs) -> list:
        return [
            link.get_attribute('href')
            for link in self.wait.until(
                lambda d: d.find_elements(
                    By.XPATH, "//th[normalize-space()='affluenza finale']//ancestor::tbody/tr//child::a")
            )
        ]

class EstraiProvinceFromSelectV1(EstraiProvince):
    def estrai_province(self, **kwargs) -> list:
        options: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(
                By.XPATH, "//option[not(contains(text(), 'scegli'))]"
            )
        )

        return [
            re.sub(r'000001', option.get_attribute('value'), self.driver.current_url)
            for option in options
        ]

class EstraiProvinceFromSelectV2(EstraiProvince):
    def estrai_province(self, **kwargs) -> list:
        options: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(
                By.XPATH, "//option[not(contains(text(), 'Scegli'))]"
            )
        )

        return [
            re.sub(r'__1.html', f"{option.get_attribute('value')}.html", self.driver.current_url)
            for option in options
        ]


class EstraiAffluenza(ABC, WebScraper):
    @abstractmethod
    def estrai_affluenza(self, **kwargs) -> dict:
        pass


class EstraiAffluenzaFromApp(EstraiAffluenza):
    def estrai_affluenza(self, **kwargs) -> dict:
        self.wait.until(EC.element_to_be_clickable((
            By.XPATH, "//div[normalize-space()='AFFLUENZA']"
        ))).click()

        self.wait.until(EC.element_to_be_clickable((
            By.XPATH,
            "//mat-panel-title[normalize-space()='finale']"
        ))).click()

        tabella_affluenza = self.driver.find_element(
            By.TAG_NAME, 'table'
        ).get_attribute('outerHTML')
        affluenza_df = pd.read_html(tabella_affluenza, header=0)
        return {
            'votanti': int(affluenza_df[0].votanti),
            'elettori': int(affluenza_df[0].elettori)
        }


class EstraiAffluenzaFromTable(EstraiAffluenza, ABC):

    def __init__(self, driver, wait=False, votanti_index=1, elettori_index=14) -> None:
        super().__init__(driver)
        self.wait_staleness = wait
        self.votanti_index = votanti_index
        self.elettori_index = elettori_index

    def estrai_affluenza(self, **kwargs) -> dict:
        self.driver.get(kwargs['url'])

        if self.wait_staleness:
            self.wait.until(EC.staleness_of(self.driver.find_element(
                By.XPATH, "//*[@id='contenitoreMainAll']")))

        rows: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(
                By.XPATH, "//table[contains(normalize-space(), 'finale') or contains(normalize-space(), 'FINALE')]//child::tr")
        )[2:]

        data = {}
        for row in rows:
            cell = row.find_elements(By.TAG_NAME, 'td')
            if len(cell) == 0 or cell[0].text == "TOTALE":
                continue

            data[cell[0].text] = {
                'votanti': self.atoi(cell[self.votanti_index].text),
                'elettori': self.atoi(cell[self.elettori_index].text)
            }

        return data
