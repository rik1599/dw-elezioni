"""
Script python per scaricare i dati delle elezioni comunali da
http://elezionistorico.regione.fvg.it (elezioni del 2019)
(usa selenium)
"""
import re

from selenium.webdriver.common.by import By
from selenium.webdriver.remote import webdriver

from .downloader_base import FvgElectionDownloaderComunali, FvgElectionsDownloaderRegionali
from .estrai_affluenza import EstraiAffluenza, EstraiProvince
from .estrai_comuni import EstraiComuni
from .estrai_liste import EstraiListe
from .estrai_presidente import EstraiPresidente


class LinksMixin:
    """
    Estrae alcuni link dalle pagine
    """
    @staticmethod
    def link_page_sindaco_presidente(driver: webdriver.WebDriver):
        return driver.find_element(
            By.XPATH,
            "//a[contains(normalize-space(), 'Sindaco') or contains(normalize-space(), 'Presidente')]"
        ).get_attribute('href')

    @staticmethod
    def link_page_liste(driver: webdriver.WebDriver):
        link_liste = driver.find_elements(
            By.XPATH,
            "//a[contains(normalize-space(), 'Risultati') and not(contains(text(), 'Turno'))]"
        )

        if len(link_liste) > 0:
            return link_liste[0].get_attribute('href')

        return None

    @staticmethod
    def link_page_affluenza(driver: webdriver.WebDriver):
        return driver.find_element(
            By.XPATH,
            "//a[contains(normalize-space(), 'Affluenza')]"
        ).get_attribute('href')


class FvgElectionsDownloaderV2Comunali(FvgElectionDownloaderComunali):
    """
    Scarica i dati delle elezioni comunali
    """

    def estrai_dati_elezione_comune(self, comune: tuple[str, str]) -> None:
        self.driver.get(url=comune[1])
        print(comune[0])

        self.wait.until(lambda d: d.find_element(
            By.XPATH, "//*[@id='testo' or @class='contenitoreTabelle']"))
        no_data = self.driver.find_elements(By.XPATH, "//td[@class='error']")
        if len(no_data) > 0:
            return

        link_affluenza = LinksMixin.link_page_affluenza(self.driver)

        link_sindaco_presidente = LinksMixin.link_page_sindaco_presidente(
            self.driver)

        data = {
            "sindaco": self.estrai_presidente.estrai_presidente(url=link_sindaco_presidente),
            "affluenza": list(self.estrai_affluenza.estrai_affluenza(url=link_affluenza).items())[0][1],
        }

        link_liste = LinksMixin.link_page_liste(self.driver)
        if link_liste is not None:
            data["liste"] = self.estrai_liste.estrai_liste(url=link_liste)

        nome_comune = re.sub(r' ballottaggio| 1(°|\\u00b0) turno', '', comune[0])
        if "ballottaggio" in comune[0]:
            self.dati_ballottaggio[nome_comune] = data
        else:
            self.dati_primo_turno[nome_comune] = data


class FvgElectionsDownloaderV2Regionali(FvgElectionsDownloaderRegionali):
    """
    Scarica i dati delle elezioni regionali
    """

    def __init__(self, driver: webdriver.WebDriver, base_url: str,
                 estrai_comuni: EstraiComuni, estrai_liste: EstraiListe,
                 estrai_presidente: EstraiPresidente, estrai_affluenza: EstraiAffluenza,
                 data_elezioni: str, estrai_link_circoscrizioni: EstraiProvince) -> None:
        super().__init__(driver, base_url, estrai_comuni, estrai_liste,
                         estrai_presidente, estrai_affluenza, data_elezioni)
        self.estrai_link_circoscrizioni = estrai_link_circoscrizioni

    def estrai_dati_elezione_comune(self, comune: tuple[str, str]) -> None:
        self.driver.get(url=comune[1])
        print(comune[0])

        self.wait.until(lambda d: d.find_element(
            By.XPATH, "//*[@id='menuSecondarioContenitoreBottoni']"))

        link_sindaco_presidente = LinksMixin.link_page_sindaco_presidente(
            self.driver)
        link_liste = LinksMixin.link_page_liste(self.driver)

        self.dati_elezioni[comune[0]] = {
            'presidente': self.estrai_presidente.estrai_presidente(url=link_sindaco_presidente),
            'liste': self.estrai_liste.estrai_liste(url=link_liste)
        }

    def estrai_affluenza_regionali(self) -> None:
        link_affluenza = LinksMixin.link_page_affluenza(self.driver)
        self.driver.get(link_affluenza)

        link_circoscrizioni: list[str] = self.estrai_link_circoscrizioni.estrai_province()

        for link in link_circoscrizioni:
            for comune, affluenza in self.estrai_affluenza.estrai_affluenza(url=link).items():
                if self.dati_elezioni.get(comune, None) is None:
                    self.dati_elezioni[comune] = {}

                self.dati_elezioni[comune]['affluenza'] = affluenza
