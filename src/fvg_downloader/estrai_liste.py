from abc import ABC, abstractmethod

import pandas as pd
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

from webscraper import WebScraper
from fvg_downloader.estrai_presidente import liste_aliases


class EstraiListe(ABC, WebScraper):
    @abstractmethod
    def estrai_liste(self, **kwargs):
        pass


class EstraiListeFromApp(EstraiListe):
    def estrai_liste(self, **kwargs):
        self.wait.until(EC.element_to_be_clickable((
            By.XPATH, "//div[normalize-space()='LISTE']"
        ))).click()

        voti_contestati: WebElement = self.wait.until(lambda d: d.find_element(
            By.XPATH,
            "//p[contains(text(), 'voti contestati')]/strong"
        ))

        voti_validi = {}
        liste = self.driver.find_elements(
            By.XPATH, "//app-comunali-liste//child::tbody/tr")
        for row in liste:
            self.scroll_to_element(row)
            cell = row.find_elements(By.TAG_NAME, 'td')

            nome_lista = self.wait.until(
                lambda d, c=cell[0]: c.find_element(By.TAG_NAME, 'img')
            ).get_attribute("alt")
            voti_lista = cell[1].find_element(
                By.TAG_NAME, 'b').get_attribute('textContent')

            cell[1].find_element(By.TAG_NAME, 'mat-icon').click()
            table_preferenze = self.wait.until(lambda d: d.find_element(
                By.XPATH, '//mat-dialog-container//child::table'
            ))
            preferenze = self.__estrai_preferenze(table_preferenze)
            self.driver.find_element(
                By.XPATH,
                '//mat-dialog-container//child::button[contains(@aria-label, "chiudi")]'
            ).click()

            voti_validi[nome_lista] = {
                'voti': voti_lista,
                'preferenze': preferenze
            }

        return {
            'voti_contestati': int(voti_contestati.get_attribute("textContent")),
            'voti_validi': voti_validi
        }

    def __estrai_preferenze(self, table_preferenze: WebElement):
        candidati_df = pd.read_html(
            table_preferenze.get_attribute('outerHTML'))[0]
        return dict(zip(candidati_df[0], candidati_df[1]))


def estrai_voti_contestati(driver: WebDriver):
    return driver.find_element(
        By.XPATH,
        "//*[contains(text(), 'Voti contestati')]"
    ).text


class EstraiListeFromTable(EstraiListe):

    def estrai_liste(self, **kwargs):
        self.driver.get(url=kwargs['url'])

        liste: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(
                By.XPATH, "//table[contains(@summary, 'risultati di lista')]/tbody/tr")
        )

        voti_contestati = estrai_voti_contestati(self.driver)

        voti_liste = {
            cell[1].text: {
                'voti': self.atoi(cell[2].text),
                'preferenze': cell[4].find_element(By.TAG_NAME, 'a').get_attribute('href')
            }
            for row in liste if (cell := row.find_elements(By.TAG_NAME, 'td'))
        }

        for data in voti_liste.values():
            data['preferenze'] = self.__estrai_preferenze(data['preferenze'])

        return {
            'voti_contestati': self.atoi(voti_contestati),
            'voti_validi': voti_liste
        }

    def __estrai_preferenze(self, url):
        self.driver.get(url=url)
        candidati: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(
                By.XPATH,
                "//th[normalize-space()='candidato' or normalize-space()='CANDIDATO']//ancestor::table/tbody/tr[not(@height)]"
            )
        )

        return {
            cell[0].text: self.atoi(cell[1].get_attribute('textContent'))
            for row in candidati if (cell := row.find_elements(By.TAG_NAME, 'td'))
        }


class EstraiListeFromDiv(EstraiListe):
    def estrai_liste(self, **kwargs):
        self.driver.get(url=kwargs['url'])

        div_partiti: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(By.XPATH, "//*[@class='partitoliste']"))
        div_voti = self.driver.find_elements(
            By.XPATH, "//*[@class='voti']")[1:]
        links_preferenze = self.driver.find_elements(
            By.XPATH, "//*[@class='aligDestra']//child::a")

        voti_contestati = estrai_voti_contestati(self.driver)

        voti_liste = {
            liste_aliases(partito.text.strip()): {
                'voti': self.atoi(voti.text),
                'preferenze': preferenze.get_attribute('href')
            }
            for partito, voti, preferenze in zip(div_partiti, div_voti, links_preferenze)
        }

        for data in voti_liste.values():
            data['preferenze'] = self.__estrai_preferenze(data['preferenze'])

        return {
            'voti_contestati': self.atoi(voti_contestati),
            'voti_validi': voti_liste
        }

    def __estrai_preferenze(self, url):
        self.driver.get(url)

        div_candidati: list[WebElement] = self.wait.until(
            lambda d: d.find_elements(By.XPATH, "//*[@class='candidato4']"))[1:]
        div_preferenze = self.driver.find_elements(
            By.XPATH, "//*[@class='preferenzeGr']")

        return {
            candidato.text: self.atoi(preferenze.text)

            for candidato, preferenze in zip(div_candidati, div_preferenze)
        }
