import re
from abc import ABC, abstractmethod

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

from webscraper import WebScraper


class EstraiComuni(ABC, WebScraper):
    @abstractmethod
    def estrai_comuni(self) -> list:
        pass


class EstraiComuniFromMat(EstraiComuni):
    def estrai_comuni(self) -> list:
        letters: list[WebElement] = self.wait.until(EC.visibility_of_all_elements_located((
            By.XPATH,
            '//mat-button-toggle-group//child::button'
        )))

        comuni = []
        for letter in letters:
            letter.click()
            comuni.extend([
                comune.get_attribute('textContent').strip()
                for comune in self.driver.find_elements(By.XPATH, '//mat-nav-list/mat-list-item')
            ])
        return comuni


class EstraiComuniFromHref(EstraiComuni):
    def estrai_comuni(self) -> list:
        comuni: list[WebElement] = self.wait.until(lambda d: d.find_elements(
            By.XPATH, "//*[@id='mainColonne']//child::a[not(contains(text(), 'Affluenza'))]"))

        return [
            (link.text, link.get_attribute("href"))
            for link in comuni
        ]


def select(driver, wait):
    comuni: list[WebElement] = wait.until(
        lambda d: d.find_elements(
            By.XPATH, "//*[contains(text(), 'Comun')]//following-sibling::select//child::option[not(contains(text(), 'scegli'))]")
    )

    return [
        (re.sub(r'^S\.', 'San', comune.get_attribute('textContent')), re.sub(r'[0-9]+.html',
         f'{cod_comune}.html', driver.current_url))
        for comune in comuni if (cod_comune := comune.get_attribute('value'))
    ]


class EstraiComuniFromSelect(EstraiComuni):

    def estrai_comuni(self) -> list:
        return select(self.driver, self.wait)


class EstraiComuniFromSelectV2(EstraiComuni):

    def estrai_comuni(self) -> list:
        self.wait.until(lambda d: d.find_element(
            By.XPATH, "//a[contains(text(), 'Candidature')]")).click()
        return select(self.driver, self.wait)
