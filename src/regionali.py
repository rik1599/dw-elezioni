from selenium import webdriver
from fvg_downloader import (elezioni_fvg_v2, elezioni_fvg_v3,
                             estrai_affluenza, estrai_comuni, estrai_liste,
                             estrai_presidente)

def regionali_2018(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Regionali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2018/000497_Reg/Coalizioni/000001.html',
        estrai_comuni.EstraiComuniFromSelect(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, wait=True, votanti_index=5, elettori_index=6),
        '2018-04-29',
        estrai_affluenza.EstraiProvinceFromTable(driver)
    )

def regionali_2013(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Regionali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2013/000377_Reg/Coalizioni/000001.html',
        estrai_comuni.EstraiComuniFromSelect(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=1, elettori_index=14),
        '2013-04-21',
        estrai_affluenza.EstraiProvinceFromSelectV1(driver)
    )

def regionali_2008(driver: webdriver.Remote):
    return elezioni_fvg_v3.FvgElectionsDownloaderV3Regionali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2008/elezReg.html',
        estrai_comuni.EstraiComuniFromSelect(driver),
        estrai_liste.EstraiListeFromDiv(driver),
        estrai_presidente.EstraiPresidenteFromDiv(driver, estrai_presidente.EstraiListeCollegateFromDiv()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, elettori_index=1, votanti_index=14),
        '2008-04-13'
    )
