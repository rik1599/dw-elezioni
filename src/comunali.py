from selenium import webdriver

from fvg_downloader import (elezioni_fvg_v1, elezioni_fvg_v2, elezioni_fvg_v3,
                             estrai_affluenza, estrai_comuni, estrai_liste,
                             estrai_presidente)


def comunali_2022(driver: webdriver.Remote):
    return elezioni_fvg_v1.FvgElectionsDownloaderV1(
        driver,
        "https://elezioni.regione.fvg.it/consultazioni/ELZ_COM/2022-06-12",
        '2022-06-12',
        '2022-06-26'
    )

def comunali_2021(driver: webdriver.Remote):
    return elezioni_fvg_v1.FvgElectionsDownloaderV1(
        driver,
        "https://elezioni.regione.fvg.it/consultazioni/ELZ_COM/2021-10-03",
        '2021-10-03',
        '2021-10-17'
    )

def comunali_2020(driver: webdriver.Remote):
    return elezioni_fvg_v1.FvgElectionsDownloaderV1(
        driver,
        "https://elezioni.regione.fvg.it/consultazioni/ELZ_COM/2020-09-20",
        '2020-09-20',
        '2020-10-04'
    )

def comunali_2019(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2019/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=5, elettori_index=6),
        '2019-05-26',
        '2019-06-09'
    )

def comunali_2018(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2018/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=5, elettori_index=6),
        '2018-04-29',
        '2018-05-13'
    )

def comunali_2017(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2017/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=11, elettori_index=1),
        '2017-06-11',
        '2017-06-25'
    )

def comunali_ottobre_2016(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2016ottobre/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=11, elettori_index=1),
        '2016-10-23',
        '2016-11-06'
    )

def comunali_2016(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2016/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=11, elettori_index=1),
        '2016-06-05',
        '2016-06-19'
    )

def comunali_2015(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2015/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=11, elettori_index=1),
        '2015-05-31',
        '2015-06-14'
    )

def comunali_2014(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2014/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=11, elettori_index=1),
        '2014-05-25',
        '2014-06-08'
    )

def comunali_2013(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2013/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2013-04-21',
        '2013-05-05'
    )

def comunali_2012(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2012/',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2012-05-06',
        '2012-05-20'
    )

def comunali_2011(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2011/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2011-05-15',
        '2011-05-29'
    )

def comunali_2010(driver: webdriver.Remote):
    return elezioni_fvg_v2.FvgElectionsDownloaderV2Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/amministrative2010/',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2010-05-16',
        '2010-05-30'
    )

def comunali_2009(driver: webdriver.Remote):
    return elezioni_fvg_v3.FvgElectionsDownloaderV3Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2009/elezCom.html',
        estrai_comuni.EstraiComuniFromHref(driver),
        estrai_liste.EstraiListeFromTable(driver),
        estrai_presidente.EstraiPresidenteFromTable(driver, estrai_presidente.EstraiListeCollegateFromP()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2009-06-06'
    )

def comunali_2008(driver: webdriver.Remote):
    return elezioni_fvg_v3.FvgElectionsDownloaderV3Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2008/elezCom.html',
        estrai_comuni.EstraiComuniFromSelectV2(driver),
        estrai_liste.EstraiListeFromDiv(driver),
        estrai_presidente.EstraiPresidenteFromDiv(driver, estrai_presidente.EstraiListeCollegateFromDiv()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2008-04-13'
    )

def comunali_2008_bal(driver: webdriver.Remote):
    return elezioni_fvg_v3.FvgElectionsDownloaderV3Comunali(
        driver,
        'http://elezionistorico.regione.fvg.it/elezioni2008/elezComB.html',
        estrai_comuni.EstraiComuniFromSelectV2(driver),
        estrai_liste.EstraiListeFromDiv(driver),
        estrai_presidente.EstraiPresidenteFromDiv(driver, estrai_presidente.EstraiListeCollegateFromDiv()),
        estrai_affluenza.EstraiAffluenzaFromTable(driver, votanti_index=14, elettori_index=1),
        '2008-04-27'
    )
