import re

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

SCROLL_TO_ELEMENT_SCRIPT = 'arguments[0].scrollIntoView(true);'


class WebScraper:
    driver: WebDriver
    wait: WebDriverWait

    def __init__(self, driver: WebDriver) -> None:
        self.driver = driver
        self.wait = WebDriverWait(driver=driver, timeout=10)

    def atoi(self, string: str) -> int:
        """
        Trova in una stringa un numero e lo converte in intero
        """
        num = re.search(r'[0-9.]+', string)
        if num is None:
            return 0
        return int(num.group().replace('.', ''))

    def scroll_to_element(self, element: WebElement):
        self.driver.execute_script(SCROLL_TO_ELEMENT_SCRIPT, element)
        self.wait.until(EC.visibility_of(element))
