from selenium import webdriver
from eligendo_downloader.downloader_base import set_type, get_data_elezioni_in_range
from eligendo_downloader.eligendo_downloader_europee import EligendoDownloaderEuropee

ELIGENDO_ARCHIVIO = "https://elezionistorico.interno.gov.it/index.php"

def europee_2019(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Europee')
    data = get_data_elezioni_in_range(driver, 2019, 2019)[0]
    return EligendoDownloaderEuropee(driver, url, data)

def europee_2014(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Europee')
    data = get_data_elezioni_in_range(driver, 2014, 2014)[0]
    return EligendoDownloaderEuropee(driver, url, data)


def europee_2009(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Europee')
    data = get_data_elezioni_in_range(driver, 2009, 2009)[0]
    return EligendoDownloaderEuropee(driver, url, data)

