import re

from abc import ABC
from selenium.webdriver.common.by import By

from eligendo_downloader.downloader_base import EligendoDownloaderBase


class EligendoDownloaderV2(EligendoDownloaderBase, ABC):
    """
    Classe astratta per estrarre i dati elettorali delle elezioni 2013-2008
    """

    def download(self):
        super().download()
        self.set_select_value_by_pattern(
            "//*[@id='sel_sezione2']", re.compile(r'FRIULI.VENEZIA.GIULIA'))

        for colleggio in self.get_colleggi():
            self.set_select_value("//*[@id='sel_sezione3']", colleggio)
            for comune in self.get_comuni_colleggio():
                print(comune)
                self.set_select_value("//*[@id='sel_sezione4']", comune)
                self.dati_elezioni[comune] = self.estrai_dati_elezione_comune()

    def get_colleggi(self):
        return [
            option.text
            for option in self.driver.find_elements(By.XPATH, "//*[@id='sel_sezione3']/option[not(@value='0')]")
        ]

    def get_comuni_colleggio(self):
        return [
            option.text
            for option in self.driver.find_elements(By.XPATH, "//*[@id='sel_sezione4']/option[not(@value='0')]")
        ]

    def estrai_dati_elezione_comune(self) -> dict:
        rows = self.driver.find_elements(
            By.XPATH, "//table[contains(@summary, 'Risultat')]/tbody/tr")

        voti_liste = {}
        for row in rows:
            if row.get_attribute('class') == '':
                cell = row.find_elements(By.TAG_NAME, 'td')
                lista = row.find_element(By.TAG_NAME, 'th').text
                voti = self.atoi(cell[4].text)
                voti_liste[lista] = voti

        return {
            'affluenza': self.estrai_affluenza(),
            'schede': self.estrai_schede_non_valide(),
            'voti_validi': voti_liste
        }


class EligendoDownloaderV2Camera(EligendoDownloaderV2):
    """
    Classe per estrarre i dati elettorali delle elezioni alla Camera del 2018
    """

    def __init__(self, driver, base_url: str, data_elezioni: str) -> None:
        super().__init__(driver, base_url, data_elezioni, "camera")


class EligendoDownloaderV2Senato(EligendoDownloaderV2):
    """
    Classe per estrarre i dati elettorali delle elezioni alla Camera del 2018
    """

    def __init__(self, driver, base_url: str, data_elezioni: str) -> None:
        super().__init__(driver, base_url, data_elezioni, "senato")
