import re

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from eligendo_downloader.downloader_base import EligendoDownloaderBase


class EligendoDownloaderEuropee(EligendoDownloaderBase):
    """
    Downloader per elezioni europee
    """

    def __init__(self, driver, base_url: str, data_elezioni: str) -> None:
        super().__init__(driver, base_url, data_elezioni, 'europee')

    def download(self):
        self.driver.get(self.base_url)

        self.set_select_value(
            "//*[@id='sel_date']", self.data_elezioni)

        self.set_select_value(
            "//*[@id='sel_aree']", 'ITALIA')

        self.set_select_value_by_pattern(
            "//*[@id='sel_sezione2']", re.compile(r"ITALIA NORD-ORIENTALE"))

        self.set_select_value_by_pattern(
            "//*[@id='sel_sezione3']", re.compile(r"FRIULI.VENEZIA.GIULIA"))

        for colleggio in self.get_province():
            self.set_select_value("//*[@id='sel_sezione4']", colleggio)
            for comune in self.get_comuni_colleggio():
                print(comune)
                self.set_select_value("//*[@id='sel_sezione5']", comune)
                self.dati_elezioni[comune] = self.estrai_dati_elezione_comune()

    def get_province(self):
        return [
            option.text
            for option in self.driver.find_elements(By.XPATH, "//*[@id='sel_sezione4']/option[not(@value='0')]")
        ]

    def get_comuni_colleggio(self):
        return [
            option.text
            for option in self.driver.find_elements(By.XPATH, "//*[@id='sel_sezione5']/option[not(@value='0')]")
        ]

    def estrai_dati_elezione_comune(self) -> dict:
        rows: list[WebElement] = self.wait.until(EC.visibility_of_all_elements_located(
            (By.XPATH, "//table[contains(@summary, 'Risultat')]/tbody/tr")
        ))

        voti_liste = {}
        for row in rows:
            if row.get_attribute('class') == '':
                self.wait.until(EC.element_to_be_clickable(row))
                cell = row.find_elements(By.TAG_NAME, 'td')
                lista_cell = row.find_element(By.TAG_NAME, 'th')
                lista = lista_cell.text
                voti = self.atoi(cell[1].text)

                voti_liste[lista] = {
                    'voti': voti,
                    'preferenze': self.__estrai_preferenze(lista_cell.find_element(By.TAG_NAME, 'a'))
                }

        return {
            'affluenza': self.estrai_affluenza(),
            'schede': self.estrai_schede_non_valide(),
            'voti_validi': voti_liste
        }

    def __estrai_preferenze(self, href: WebElement) -> dict:
        self.driver.execute_script(href.get_attribute('href'))

        preferenze = {}
        rows_candidati: list[WebElement] = self.wait.until(
            EC.visibility_of_all_elements_located((By.XPATH, "//table[@summary='Elenco candidati']/tbody/tr"))
        )
        for row in rows_candidati:
            if row.get_attribute('class') == '':
                candidato = row.find_element(By.TAG_NAME, 'th').text
                voti = self.atoi(row.find_elements(By.TAG_NAME, 'td')[2].text)
                preferenze[candidato] = voti

        self.driver.find_element(By.XPATH, "//button[@class='close']").click()

        return preferenze
