import re
import json

from abc import ABC

from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.remote import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as EC


from webscraper import WebScraper


def set_type(driver: webdriver.WebDriver, tipo_elezione: str):
    radio = driver.find_element(By.ID, tipo_elezione.lower())
    radio.click()
    return driver.current_url


def get_data_elezioni_in_range(driver: webdriver.WebDriver, da_anno: int, a_anno: int):
    start_date = datetime.strptime(f"1/1/{da_anno}", '%d/%m/%Y')
    end_date = datetime.strptime(f"31/12/{a_anno}", '%d/%m/%Y')

    select_element = driver.find_element(By.ID, 'sel_date')
    select = Select(select_element)
    options = [datetime.strptime(data.get_attribute(
        'value'), '%d/%m/%Y') for data in select.options[1:]]

    return [
        datetime.strftime(data, '%d/%m/%Y')
        for data in options if start_date <= data <= end_date
    ]


class EligendoDownloaderBase(WebScraper, ABC):

    def __init__(self, driver: webdriver.WebDriver, base_url: str, data_elezioni: str, tipo_elezioni: str) -> None:
        super().__init__(driver)
        self.dati_elezioni = {}
        self.base_url = base_url
        self.data_elezioni = data_elezioni
        self.tipo_elezioni = tipo_elezioni

    def set_select_value(self, xpath: str, option_text: str):
        select_element = self.driver.find_element(By.XPATH, xpath)

        select = Select(select_element)
        select.select_by_visible_text(option_text)

    def set_select_value_by_pattern(self, xpath: str, pattern: re.Pattern):
        select_element = self.driver.find_element(By.XPATH, xpath)

        select = Select(select_element)
        for option in select.options:
            if pattern.search(option.text):
                select.select_by_visible_text(option.text)
                break

    def download(self):
        self.driver.get(self.base_url)
        self.set_select_value("//*[@id='sel_date']", self.data_elezioni)
        self.set_select_value_by_pattern(
            "//*[@id='sel_aree']", re.compile(r"^ITALIA"))

    def estrai_affluenza(self) -> dict:
        elettori = self.driver.find_element(
            By.XPATH, "//*[text()='Elettori']//following-sibling::td")
        votanti = self.driver.find_element(
            By.XPATH, "//*[text()='Votanti']//following-sibling::td")

        return {
            'elettori': self.atoi(elettori.text),
            'votanti': self.atoi(votanti.text)
        }

    def estrai_schede_non_valide(self) -> dict:
        bianche = self.driver.find_element(
            By.XPATH, "//*[text()='Bianche']//following-sibling::td")
        non_valide = self.driver.find_element(
            By.XPATH, "//*[contains(text(), 'Non valide')]//following-sibling::td")

        return {
            'bianche': self.atoi(bianche.text),
            'non_valide': self.atoi(non_valide.text) - self.atoi(bianche.text)
        }

    def to_json(self, path):
        data_elezioni = datetime.strptime(
            self.data_elezioni, '%d/%m/%Y').strftime('%Y-%m-%d')
        with open(f"{path}{self.tipo_elezioni}_{data_elezioni}.json", 'w', encoding='UTF-8') as file:
            json.dump(self.dati_elezioni, file, indent=2)
