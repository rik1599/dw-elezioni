from selenium import webdriver
from eligendo_downloader.downloader_base import set_type, get_data_elezioni_in_range
from eligendo_downloader.eligendo_downloader_v2 import EligendoDownloaderV2Camera, EligendoDownloaderV2Senato
from eligendo_downloader.eligendo_dowloader_v1 import EligendoDownloaderV1Camera, EligendoDownloaderV1Senato

ELIGENDO_ARCHIVIO = "https://elezionistorico.interno.gov.it/index.php"

def camera_2018(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Camera')
    data = get_data_elezioni_in_range(driver, 2018, 2018)[0]
    return EligendoDownloaderV1Camera(driver, url, data)

def camera_2013(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Camera')
    data = get_data_elezioni_in_range(driver, 2013, 2013)[0]
    return EligendoDownloaderV2Camera(driver, url, data)

def camera_2008(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Camera')
    data = get_data_elezioni_in_range(driver, 2008, 2008)[0]
    return EligendoDownloaderV2Camera(driver, url, data)

def senato_2018(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Senato')
    data = get_data_elezioni_in_range(driver, 2018, 2018)[0]
    return EligendoDownloaderV1Senato(driver, url, data)

def senato_2013(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Senato')
    data = get_data_elezioni_in_range(driver, 2013, 2013)[0]
    return EligendoDownloaderV2Senato(driver, url, data)

def senato_2008(driver: webdriver.Remote):
    driver.get(ELIGENDO_ARCHIVIO)
    url = set_type(driver, 'Senato')
    data = get_data_elezioni_in_range(driver, 2008, 2008)[0]
    return EligendoDownloaderV2Senato(driver, url, data)