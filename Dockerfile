FROM continuumio/miniconda3 AS builder

ADD environment.yml /tmp/environment.yml

RUN conda init bash && \
    . ~/.bashrc && \
    conda update conda && \
    conda env update -f /tmp/environment.yml && \
    conda activate base

FROM builder

WORKDIR /app
COPY . /app/

ENV PYTHONPATH=src/
ENV ATOTI_DISABLE_TELEMETRY=true
ENV ATOTI_HIDE_EULA_MESSAGE=true

RUN echo "conda activate base" >> ~/.bashrc

ENTRYPOINT [ "/bin/bash", "-l", "-c" ]
CMD [ "tail -f /dev/null" ]

EXPOSE 80
EXPOSE 8888