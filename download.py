import os
import sys

from selenium import webdriver

from src import comunali, europee, politiche, regionali

def download_explicit(selenium):
    for arg in sys.argv[1:]:
        module,func = arg.split('.')
        call = getattr(globals()[module], func)
        print(arg)
        downloader = call(selenium)
        downloader.download()
        downloader.to_json('data/')

def download_all(selenium):
    modules = [comunali, europee, politiche, regionali]
    for mod in modules:
        for i in dir(mod):
            item = getattr(mod, i)
            if callable(item):
                print(str(item))
                downloader = item(selenium)
                downloader.download()
                downloader.to_json('data/')

def main():
    options = {
        'firefox': webdriver.FirefoxOptions(),
        'chrome': webdriver.ChromeOptions()
    }

    with webdriver.Remote(
        command_executor=os.environ.get("SELENIUM_GRID_URL", "http://localhost:4444"), 
        options=options[os.environ.get("BROWSER", 'firefox')]) as selenium:
        if '--all' in sys.argv:
            download_all(selenium)
        else:
            download_explicit(selenium)

if __name__ == "__main__":
    main()
