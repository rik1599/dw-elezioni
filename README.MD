# Data Warehouse elezioni in Friuli Venezia-Giulia dal 2008 al 2022

Data warehouse con i dati delle elezioni in Friuli Venezia Giulia dal 2008 al 2022.
In particolare vengono raccolti i dati delle elezioni

- Politiche

- Regionali

- Europee

- Comunali

## Modello entità-relazioni
![](./diagrams/e-r-elezioni.drawio.svg)

## Modello DFM dei dati
![](./diagrams/dfm.drawio.svg)

## Schema SQL
![](./diagrams/sql-scheme.drawio.svg)

## Fonti dei dati

- [Archivio storico di Eligendo](https://elezionistorico.interno.gov.it/)

- [Risultati elettorali FVG](https://autonomielocali.regione.fvg.it/aall/opencms/AALL/Elezioni/risultati/)

## Esempi di Query OLAP

- Numero di schede bianche nelle elezioni alla Camera dal 1992 al 2018

- Differenza tra i voti ottenuti dai candidati presidente e la somma dei voti delle liste ad essi collegate

- Il candidato al consiglio che ha ottenuto più voti nelle elezioni comunali di Trieste

- Rapporto tra numero dei voti validi e numero degli elettori aggregata per candidatura, comune ed elezione

- Qual è il comune in cui un determinato candidato ha preso più preferenze

- Qual è la media di preferenze nelle elezioni regionali di un determinato candidato in un determinato comune

## Tecnologie utilizzate

- [Pandas](https://pandas.pydata.org/)

- [Selenium](https://www.selenium.dev/)

- [Atoti](https://www.atoti.io/)

- [Conda](https://docs.conda.io/en/latest/)

## Come utilizzare questo repository

Si esegue il comando:
```
docker compose up -d
```
Verranno in automatico creati due container:

1. `dwelezioni`: contiene il codice python e il webserver per atoti,

2. `selenium`: contiene il browser firefox utilizzato dagli script di webscraping.

Inoltre le directory `data/` e `clean/` del repository vengono montate come volumi all'interno del container `dwelezioni`.

### Scaricare i dati
Per scaricare TUTTI i dati in formato json
```
docker compose exec dwelezioni python download.py --all
```

Per scaricare solo singole elezioni
```
docker compose exec dwelezioni python download.py <elezione>...<elezione>
```
(Se ne possono indicare più di uno)

Valori possibili per `<elezione>`:
- comunali.comunali_[2008-2022]
- comunali.comunali_2008_bal (ballottaggio del 2008)
- regionali.regionali_[2008,2013,2018]
- camera.camera_[2008,2013,2018]
- senato.senato_[2008,2013,2018]
- europee.europee_[2009,2014,2019]

I file JSON vengono salvati nella directory `data/` del repository.

### Pulizia e normalizzazione dei dati
```
docker compose exec dwelezioni python clean.py data/ <csv|sqlite> clean/
```

Prende tutti i file JSON dalla directory `data/` e normalizza i dati nel formato indicato nel secondo parametro:

- csv: genera tre file csv (`risultati_elettorali.csv`, `elezioni.csv`, `anagrafica.csv`) nella directory `clean/`

- sqlite: genera un database SQLite (`dw-elezioni.sqlite`) nella directory `clean/`

In entrambi i casi i dati in uscita seguono lo schema indicato in questa [immagine](#schema-sql).

### Interfaccia web per la navigazione
```
docker compose exec dwelezioni python atoti_ui.py clean/
```
Costruisce i cubi OLAP a partire dai tre file csv generati in fase di normalizzazione dei dati.

Nel primo parametro va indicata la directory in cui sono memorizzati i file.

Aprire un browser web all'indirizzo [http://localhost:8080]()

Attualmente l'interfaccia web è pensata solo per esplorare il data warehouse. Qualsiasi query, tabella o grafico creato verrà eliminato nel momento in cui il webserver viene terminato.

### Jupyter lab
C'è la possibilità di esplorare la repository attraverso Jupyter Lab:
```
docker compose exec dwelezioni jupyter lab --allow-root
```