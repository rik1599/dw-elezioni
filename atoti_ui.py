import sys
import atoti as tt
import pandas as pd
import numpy as np

# Apre la sessione
logging_config = tt.LoggingConfig(destination=sys.stdout)
session = tt.Session(logging=logging_config, port=80)

# Crea le tabelle su cui costruire i due cubi
risultati_elettorali = session.read_csv('clean/risultati_elettorali.csv', keys=['id'])
elezioni = session.read_csv('clean/elezioni.csv', keys=['id'])

# Legge il csv con l'anagrafica utilizzando pandas e lo converte in due tabelle distinte
# le quali vengono joinate sui risultati_elettorali
anagrafica_df = pd.read_csv('clean/anagrafica.csv', dtype={'id': np.int32})
anagrafica1 = session.read_pandas(anagrafica_df, table_name="Anagrafica", keys=['id'])
anagrafica2 = session.read_pandas(anagrafica_df, table_name="Anagrafica2", keys=['id'])

risultati_elettorali.join(anagrafica1, mapping={'candidato': 'id'})
risultati_elettorali.join(anagrafica2, mapping={'candidato_sostenuto': 'id'})

# Imposta i valori di default per l'anno (di default è NULL, ma così facendo non è possibile costruirci una gerarchia)
risultati_elettorali['anno'].default_value = 1900
elezioni['anno'].default_value = 1900

cube_risultati_elettorali = session.create_cube(risultati_elettorali)
m, h = cube_risultati_elettorali.measures, cube_risultati_elettorali.hierarchies

# Elimina alcune gerarchie create in automatico da atoti
del h['id']
del h['giorno']
del h['Anagrafica', 'nome']
del h['Anagrafica2', 'nome']
del h['lista']

# Costruisce le nuove gerarchie
h['Data_elezione'] = [
    risultati_elettorali['giorno'],
    risultati_elettorali['anno']
]

h['Lista'] = [
    risultati_elettorali['lista'],
    anagrafica2['nome'],
]

h['Candidato'] = [
    anagrafica1['nome'],
    risultati_elettorali['lista'],
]

# Crea una nuova misura aggregando sul massimo
m['num_voti.MAX'] = tt.agg.max(risultati_elettorali['num_voti'])

del m['anno.MEAN']
del m['anno.SUM']
del m['candidato.MEAN']
del m['candidato.SUM']
del m['candidato_sostenuto.MEAN']
del m['candidato_sostenuto.SUM']

cube_elezioni = session.create_cube(elezioni)
m, h = cube_elezioni.measures, cube_elezioni.hierarchies

del h['id']
del h['giorno']

h['Data_elezione'] = [
    elezioni['giorno'],
    elezioni['anno']
]

del m['anno.MEAN']
del m['anno.SUM']

print("Atoti session is ready!")
session.wait()
